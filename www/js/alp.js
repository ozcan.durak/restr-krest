function goHome()
{
    popPage();
    popPage();
    removeStorage('infinite_category');
    // added to directly redirect to ony active product - alp
    ajaxCall2('loadItemByCategory',"cat_id=1",function (data) {
        active_product_id = data.details.data[0].item_id;
        loadPage('item_details.html');
        params = "item_id="+ active_product_id + "&cat_id=1";
        ajaxCall('loadItemDetails', params );
    });

}
function showOrders() {
    loadPage('orders.html');
    setStorage("next_step",'home_page');
    if(!isLogin()){
        $(".show_if_login").hide();
        $(".show_if_notlogin").show();
    } else {
        $(".show_if_notlogin").hide();
        $(".show_if_login").show();
        paginate_count=1;
        paginate_result=0;
        ajaxCall('getOrders', '');
    }
}

function openProfile() {

    showPage('profile_menu.html');
    // it's not best practices but not found any other solution for now
    setTimeout(function () {
        if(!isLogin()){
            setStorage("next_step",'home_page');
            $(".show_if_login").hide();
            //$(".show_if_notlogin").show();
            $(".show_if_notlogin2").css({"display":"inline-flex"});
            $(".profile_name").html('');
        } else {
            $(".show_if_notlogin2").hide();
            $(".show_if_login").show();
            ajaxCall('getUserProfile', '');
        }

        if(settings = AppSettings()){
            if(empty(settings.singleapp_help_url)){
                $(".help_menu").hide();
            }
        }
        translatePage();
    },500);
}
function ajaxCallAlp(action, data,callback  ) {

    var ajax_uri = ajax_url + "/" + action;

    data += "&merchant_keys=" + krms_config.MerchantKeys;
    data += "&device_id=" + device_id;
    data += "&device_platform=" + device_platform;

    token = getStorage("token");
    if (!empty(token)) {
        data += "&token=" + token;
    }

    transaction_type = $(".transaction_type").val();
    if (!empty(transaction_type)) {
        data += "&transaction_type=" + transaction_type;
    }

    data += "&lang=" + getLangCode();


    dump("METHOD=>" + action);
    dump(ajax_uri + "?" + data);

    var ajax_request = $.ajax({
        url: ajax_uri,
        method: "GET",
        data: data,
        dataType: "jsonp",
        timeout: 30000,
        crossDomain: true,
        beforeSend: function (xhr) {
            dump("before send ajax");

            clearTimeout(timer);

            if (ajax_request != null) {
                ajax_request.abort();
                clearTimeout(timer);
            } else {
                showLoader(true);

                timer = setTimeout(function () {
                    if (ajax_request != null) {
                        ajax_request.abort();
                    }
                    showLoader(false);
                    showToast(t('Request taking lot of time. Please try again'));
                }, 30000);
            }
        }
    });

    /*DONE*/
    ajax_request.done(function (data) {
        dump("done ajax");
        dump(data);
        showLoader(false);

        if (data.code == 1) {
            callback(data);
        }
    })
}